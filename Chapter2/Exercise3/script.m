%% Constants
% Experiment size
N = 10000;

% Sigmas and rho
sigmaX = 1;
sigmaY = 1;
rho = 0.8;
cov = rho*sqrt(sigmaX*sigmaY);

% Constants for mvnrnd
SIGMA = [sigmaX  cov    ;
         cov     sigmaY];
M = [0 0];

%% Code
% Create the random sample
R = mvnrnd(M,SIGMA,N);

% Seperate
X = R(:,1);
Y = R(:,2);

% Calculate var
varX = var(X);
varY = var(Y);
varXY = var(X+Y);

%% Display results
fprintf('Experiment:\n');
fprintf('Var[X+Y]      = %f\n', varXY);
fprintf('Var[X]+Var[Y] = %f\n', varX+varY);

fprintf('Theory:\n');
fprintf('Var[X] = %f\n', sigmaX);
fprintf('Var[Y] = %f\n', sigmaY);
fprintf('rho    = %f\n', rho);

fprintf('Var[X+Y] = Var[X] + Var[Y] + 2*Cov(X,Y)\n');
fprintf('         = %f + %f + 2*%f = %f\n', sigmaX, sigmaY, cov, sigmaX+sigmaY+2*cov);
