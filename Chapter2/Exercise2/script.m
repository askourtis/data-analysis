%% Code
% Create a distribution object
DO = makedist('Exponential', 1);

% Create a sample from uniform distribution in [0, 1]
U = rand(10000, 1);
% Convert it to a sample of the selected distribution
R = icdf(DO, U);

% Create a linspace to plot the actual pdf
%X = linspace(icdf(pd, 0.01), icdf(pd, 0.99));
X = linspace(min(R), max(R));
Y = pdf(DO, X);

%% Plot
figure()
title('Histogram')
hold on
grid on
histogram(R, 'Normalization', 'pdf')
plot(X, Y, 'k','LineWidth', 2);
xlabel('Value')
ylabel('Probability density')
