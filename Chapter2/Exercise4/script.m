%% Constants
% Experiment size
N = 1000000;

% Define domain [a, b]
a = 1;
b = 2;

% Theoretical values
f = 1/(b-a);
Mx = (b^2-a^2)/2 * f;
My = (log(b)-log(a)) * f;

%% Code
% Compute random samples
U = rand(N,1);
X = (b-a) .* U + a;
Y = 1 ./ X;


Ex = mean(X);
Ey = mean(Y);

%% Display
fprintf('Experiment:\n');
fprintf('E[X] = %f\n', Ex);
fprintf('1/E[X] = %f\n', 1/Ex);
fprintf('E[1/X] = %f\n', Ey);

fprintf('Theory:\n');
fprintf('E[X] = %f\n', Mx);
fprintf('1/E[X] = %f\n', 1/Mx);
fprintf('E[1/X] = %f\n', My);