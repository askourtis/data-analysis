%% Constants
% Probability threshold
t = 0.5;

% Logspace for variable experiment size
% Base
b = 2;
% Exponents
e = (0:14)';
% Final logspace
R = b.^e;

% Number of experiments
N = length(R);

%% Code
% Preallocate a vector to store the relative frequency of occurrence for
% heads
PH = nan(N, 1);
% For each experiment size
for i = 1:length(PH)
    n = R(i);
    % Generate the random sample
    S = rand(n, 1);
    % Calculate the relative frequency for the given size
    PH(i) = sum(S < t)/n;
end

% Calculate the relative frequency for tails for each experiment size
PT = 1 - PH;

%% Plot
P = [PH PT];

figure()
% Plot stacked bars
bar(P, "stacked");
hold on
grid on
% Yline at threshold
plot([0 length(PH)+1], [t, t], 'k--');

% X axis
xlabel("Experiment Size");
xticklabels(string(b) + "^{" + string(e) + "}");
% Y axis
ylabel("Relative frequency")
% Misc
legend("Heads", "Tails");
title("Coin toss experiment")
hold off
